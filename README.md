# Arduino TLC5940 Shield

[TLC5940](http://www.ti.com/lit/ds/symlink/tlc5940.pdf) is a LED driver chip, but it can be used for regular PWM applications too. In the context of Arduino, there are libraries.

- [New sources on GitHub](https://github.com/search?q=tlc5940&s=stars)
- [Old library source is on Google Code](http://code.google.com/p/tlc5940arduino/)
