EESchema Schematic File Version 4
LIBS:Arduino_TLC5940_Shield_CB-cache
EELAYER 29 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Driver_LED:TLC5940PWP U1
U 1 1 5CB6C613
P 2400 2550
F 0 "U1" H 2400 3731 50  0000 C CNN
F 1 "TLC5940PWP" H 2400 3640 50  0000 C CNN
F 2 "Arduino_TLC5940_Shield:TSSOP-28-1EP_4.4x9.7mm_Pitch0.65mm" H 2425 1575 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/tlc5940.pdf" H 2000 3250 50  0001 C CNN
	1    2400 2550
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5CB728F4
P 14650 8950
F 0 "#FLG01" H 14650 9025 50  0001 C CNN
F 1 "PWR_FLAG" V 14650 9078 50  0000 L CNN
F 2 "" H 14650 8950 50  0001 C CNN
F 3 "~" H 14650 8950 50  0001 C CNN
	1    14650 8950
	-1   0    0    1   
$EndComp
$Comp
L power:PWR_FLAG #FLG02
U 1 1 5CB73415
P 14650 8750
F 0 "#FLG02" H 14650 8825 50  0001 C CNN
F 1 "PWR_FLAG" V 14650 8878 50  0000 L CNN
F 2 "" H 14650 8750 50  0001 C CNN
F 3 "~" H 14650 8750 50  0001 C CNN
	1    14650 8750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x08 J2
U 1 1 5CB75D7D
P 13100 8850
F 0 "J2" H 13180 8842 50  0000 L CNN
F 1 "Digital IO 8-13" H 13180 8751 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x08_Pitch2.54mm" H 13100 8850 50  0001 C CNN
F 3 "~" H 13100 8850 50  0001 C CNN
	1    13100 8850
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x08 J1
U 1 1 5CB73C61
P 13100 7400
F 0 "J1" H 13180 7392 50  0000 L CNN
F 1 "Digital IO 0-7" H 13180 7301 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x08_Pitch2.54mm" H 13100 7400 50  0001 C CNN
F 3 "~" H 13100 7400 50  0001 C CNN
	1    13100 7400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 2150 950  2150
Text Label 950  2150 0    50   ~ 0
GSCLK
Wire Wire Line
	12900 7400 12150 7400
Text Label 12150 7400 0    50   ~ 0
GSCLK
Text Label 950  2450 0    50   ~ 0
XLAT
Wire Wire Line
	12900 8650 12150 8650
Text Label 12150 8650 0    50   ~ 0
XLAT
Text Label 950  2350 0    50   ~ 0
BLANK
Wire Wire Line
	12900 8750 12150 8750
Text Label 12150 8750 0    50   ~ 0
BLANK
Wire Wire Line
	12900 8850 12150 8850
Text Label 12150 8850 0    50   ~ 0
SIN_0
Text Label 950  3250 0    50   ~ 0
SIN_0
$Comp
L Device:R R1
U 1 1 5CB7DD1A
P 1200 1800
F 0 "R1" V 993 1800 50  0000 C CNN
F 1 "1K VAR" V 1084 1800 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 1130 1800 50  0001 C CNN
F 3 "~" H 1200 1800 50  0001 C CNN
	1    1200 1800
	-1   0    0    1   
$EndComp
Wire Wire Line
	1700 1950 1200 1950
$Comp
L Connector_Generic:Conn_01x02 J4
U 1 1 5CB828D0
P 14850 6050
F 0 "J4" H 14930 6042 50  0000 L CNN
F 1 "POWER" H 14930 5951 50  0000 L CNN
F 2 "Connectors_Phoenix:PhoenixContact_MSTBVA-G_02x5.00mm_Vertical" H 14850 6050 50  0001 C CNN
F 3 "~" H 14850 6050 50  0001 C CNN
	1    14850 6050
	1    0    0    -1  
$EndComp
Text Label 13450 5950 0    50   ~ 0
GND
Wire Wire Line
	12900 9050 12150 9050
Text Label 12150 9050 0    50   ~ 0
SCLK
$Comp
L Connector_Generic:Conn_01x06 J5
U 1 1 5CBCDB69
P 15100 7350
F 0 "J5" H 15180 7342 50  0000 L CNN
F 1 "ANALOG" H 15180 7251 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06_Pitch2.54mm" H 15100 7350 50  0001 C CNN
F 3 "~" H 15100 7350 50  0001 C CNN
	1    15100 7350
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x06 J6
U 1 1 5CBCF20C
P 15100 8850
F 0 "J6" H 15180 8842 50  0000 L CNN
F 1 "POWER PINS" H 15180 8751 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06_Pitch2.54mm" H 15100 8850 50  0001 C CNN
F 3 "~" H 15100 8850 50  0001 C CNN
	1    15100 8850
	1    0    0    -1  
$EndComp
Text Notes 15050 6850 0    50   ~ 0
Analog pins
Text Notes 15050 8350 0    50   ~ 0
Power pins
Text Notes 13050 6800 0    50   ~ 0
Digital pins 0 - 7
Text Notes 13000 8350 0    50   ~ 0
Digital pins 8 - 13
Wire Wire Line
	14900 8750 14750 8750
Wire Wire Line
	14750 8750 14750 8850
Wire Wire Line
	14750 8850 14900 8850
Wire Wire Line
	14450 8750 14650 8750
Connection ~ 14750 8750
Text Label 14450 8750 0    50   ~ 0
GND
Wire Wire Line
	14900 8950 14650 8950
Text Label 14450 8950 0    50   ~ 0
5V
Text Label 950  1300 0    50   ~ 0
5V
Text Label 13450 6250 0    50   ~ 0
5V_PWR
NoConn ~ 14900 7150
NoConn ~ 14900 7250
NoConn ~ 14900 7350
NoConn ~ 14900 7450
NoConn ~ 14900 7550
NoConn ~ 14900 7650
NoConn ~ 14900 8650
NoConn ~ 14900 9050
NoConn ~ 14900 9150
Wire Wire Line
	1700 2050 950  2050
Text Label 950  2050 0    50   ~ 0
5V
$Comp
L Device:C C1
U 1 1 5CC14F70
P 2300 3800
F 0 "C1" H 2415 3846 50  0000 L CNN
F 1 "0.1uF" H 2415 3755 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 2338 3650 50  0001 C CNN
F 3 "~" H 2300 3800 50  0001 C CNN
	1    2300 3800
	-1   0    0    1   
$EndComp
Wire Wire Line
	14650 6050 14550 5950
Wire Wire Line
	14650 6150 14550 6250
Text Notes 3050 1500 0    50   ~ 0
LED’s connect here
NoConn ~ 1700 2750
NoConn ~ 12900 7100
NoConn ~ 12900 7200
NoConn ~ 12900 7300
NoConn ~ 12900 7500
NoConn ~ 12900 7600
NoConn ~ 12900 7700
NoConn ~ 12900 8550
NoConn ~ 12900 8950
NoConn ~ 12900 9150
NoConn ~ 12900 9250
Wire Wire Line
	950  2450 1700 2450
Wire Wire Line
	950  2350 1700 2350
Wire Wire Line
	950  3250 1700 3250
Wire Wire Line
	950  3150 1700 3150
$Comp
L power:GND #PWR01
U 1 1 5CB7D2C2
P 1050 1650
F 0 "#PWR01" H 1050 1400 50  0001 C CNN
F 1 "GND" V 1055 1522 50  0000 R CNN
F 2 "" H 1050 1650 50  0001 C CNN
F 3 "" H 1050 1650 50  0001 C CNN
	1    1050 1650
	0    1    1    0   
$EndComp
Text Label 950  3150 0    50   ~ 0
SCLK
Connection ~ 14650 8950
Wire Wire Line
	14650 8950 14450 8950
Text Label 5650 1250 0    50   ~ 0
5V
Text Label 950  1050 0    50   ~ 0
5V_PWR
Text Label 5650 1000 0    50   ~ 0
5V_PWR
Connection ~ 14650 8750
Wire Wire Line
	14650 8750 14750 8750
Text Label 3200 2150 0    50   ~ 0
OUT3
Text Label 3200 2250 0    50   ~ 0
OUT4
Text Label 3200 2350 0    50   ~ 0
OUT5
Text Label 3200 2450 0    50   ~ 0
OUT6
Text Label 3200 2550 0    50   ~ 0
OUT7
Text Label 3200 2650 0    50   ~ 0
OUT8
Text Label 3200 2750 0    50   ~ 0
OUT9
Text Label 3200 2850 0    50   ~ 0
OUT10
Text Label 3200 2950 0    50   ~ 0
OUT11
Text Label 3200 3150 0    50   ~ 0
OUT13
Text Label 3200 3250 0    50   ~ 0
OUT14
Text Label 1450 1950 0    50   ~ 0
IREF_1
Text Label 6250 1950 0    50   ~ 0
IREF_2
Text Label 7850 3250 0    50   ~ 0
OUT30
Text Label 7850 3150 0    50   ~ 0
OUT29
Text Label 7850 3050 0    50   ~ 0
OUT28
Text Label 7850 2950 0    50   ~ 0
OUT27
Text Label 7850 2850 0    50   ~ 0
OUT26
Text Label 7850 2750 0    50   ~ 0
OUT25
Text Label 7850 2650 0    50   ~ 0
OUT24
Text Label 7850 2550 0    50   ~ 0
OUT23
Text Label 7850 2450 0    50   ~ 0
OUT22
Text Label 7850 2150 0    50   ~ 0
OUT19
Text Label 7850 2050 0    50   ~ 0
OUT18
Text Label 7850 1950 0    50   ~ 0
OUT17
Text Label 7850 1850 0    50   ~ 0
OUT16
Text Label 5650 3150 0    50   ~ 0
SCLK
Wire Wire Line
	6400 3150 5650 3150
Text Label 5650 2450 0    50   ~ 0
XLAT
Wire Wire Line
	6400 2450 5650 2450
Text Label 5650 2350 0    50   ~ 0
BLANK
Wire Wire Line
	6400 2350 5650 2350
Text Label 5650 2150 0    50   ~ 0
GSCLK
Wire Wire Line
	6400 2150 5650 2150
Text Label 5650 2050 0    50   ~ 0
5V
Wire Wire Line
	6400 2050 5650 2050
NoConn ~ 6400 2750
Wire Wire Line
	5650 3250 6400 3250
$Comp
L power:GND #PWR0101
U 1 1 5CF27377
P 5750 1650
F 0 "#PWR0101" H 5750 1400 50  0001 C CNN
F 1 "GND" V 5755 1522 50  0000 R CNN
F 2 "" H 5750 1650 50  0001 C CNN
F 3 "" H 5750 1650 50  0001 C CNN
	1    5750 1650
	0    1    1    0   
$EndComp
Wire Wire Line
	6000 1950 6400 1950
$Comp
L Device:R R2
U 1 1 5CF0CD7A
P 6000 1800
F 0 "R2" V 5793 1800 50  0000 C CNN
F 1 "1K VAR" V 5884 1800 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 5930 1800 50  0001 C CNN
F 3 "~" H 6000 1800 50  0001 C CNN
	1    6000 1800
	-1   0    0    1   
$EndComp
Text Notes 7800 1450 0    50   ~ 0
LED’s connect here
Wire Wire Line
	7800 3350 8100 3350
Wire Wire Line
	7800 3250 8100 3250
Wire Wire Line
	7800 3150 8100 3150
Wire Wire Line
	7800 3050 8100 3050
Wire Wire Line
	7800 2950 8100 2950
Wire Wire Line
	7800 2850 8100 2850
Wire Wire Line
	7800 2750 8100 2750
Wire Wire Line
	7800 2650 8100 2650
Wire Wire Line
	7800 2550 8100 2550
Wire Wire Line
	7800 2450 8100 2450
Wire Wire Line
	7800 2350 8100 2350
Wire Wire Line
	7800 2250 8100 2250
Wire Wire Line
	7800 2150 8100 2150
Wire Wire Line
	7800 2050 8100 2050
Wire Wire Line
	7800 1950 8100 1950
$Comp
L Driver_LED:TLC5940PWP U2
U 1 1 5CDA2388
P 7100 2550
F 0 "U2" H 7100 3731 50  0000 C CNN
F 1 "TLC5940PWP" H 7100 3640 50  0000 C CNN
F 2 "Arduino_TLC5940_Shield:TSSOP-28-1EP_4.4x9.7mm_Pitch0.65mm" H 7125 1575 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/tlc5940.pdf" H 6700 3250 50  0001 C CNN
	1    7100 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 1550 2400 1300
Wire Wire Line
	2400 1300 950  1300
Wire Wire Line
	1700 3350 950  3350
Text Label 950  3350 0    50   ~ 0
SIN_1
Wire Wire Line
	6400 3350 5650 3350
Wire Wire Line
	13450 5950 14000 5950
Wire Wire Line
	13450 6250 14000 6250
Wire Wire Line
	950  3650 2300 3650
Connection ~ 2300 3650
Wire Wire Line
	2300 3650 2400 3650
Wire Wire Line
	2300 3950 950  3950
Text Label 950  3650 0    50   ~ 0
GND
Text Label 950  3950 0    50   ~ 0
5V
Wire Wire Line
	1700 1850 1700 1650
Wire Wire Line
	1700 1650 1200 1650
Wire Wire Line
	1200 1650 1050 1650
Connection ~ 1200 1650
Wire Wire Line
	7100 3650 7000 3650
Wire Wire Line
	7000 3650 5650 3650
Connection ~ 7000 3650
Text Label 5650 3650 0    50   ~ 0
GND
$Comp
L Device:C C2
U 1 1 5CEF61FF
P 7000 3800
F 0 "C2" H 7115 3846 50  0000 L CNN
F 1 "0.1uF" H 7115 3755 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 7038 3650 50  0001 C CNN
F 3 "~" H 7000 3800 50  0001 C CNN
	1    7000 3800
	-1   0    0    1   
$EndComp
Wire Wire Line
	7000 3950 5650 3950
Text Label 5650 3950 0    50   ~ 0
5V
Wire Wire Line
	5750 1650 6000 1650
Wire Wire Line
	6000 1650 6400 1650
Wire Wire Line
	6400 1650 6400 1850
Connection ~ 6000 1650
Wire Wire Line
	7100 1550 7100 1250
Wire Wire Line
	7100 1250 5650 1250
$Comp
L Device:C C4
U 1 1 5CFA94DC
P 14350 6100
F 0 "C4" H 14465 6146 50  0000 L CNN
F 1 "1uF" H 14465 6055 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 14388 5950 50  0001 C CNN
F 3 "~" H 14350 6100 50  0001 C CNN
	1    14350 6100
	-1   0    0    1   
$EndComp
Connection ~ 14350 6250
Wire Wire Line
	14350 6250 14550 6250
Connection ~ 14350 5950
Wire Wire Line
	14350 5950 14550 5950
$Comp
L Device:C C3
U 1 1 5CFA9D39
P 14000 6100
F 0 "C3" H 14115 6146 50  0000 L CNN
F 1 "0.1uF" H 14115 6055 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 14038 5950 50  0001 C CNN
F 3 "~" H 14000 6100 50  0001 C CNN
	1    14000 6100
	-1   0    0    1   
$EndComp
Connection ~ 14000 6250
Wire Wire Line
	14000 6250 14350 6250
Connection ~ 14000 5950
Wire Wire Line
	14000 5950 14350 5950
Wire Wire Line
	3100 1850 3200 1850
Wire Wire Line
	3100 1950 3200 1950
Wire Wire Line
	3100 2050 3200 2050
Wire Wire Line
	3100 2150 3200 2150
Wire Wire Line
	3100 2250 3200 2250
Wire Wire Line
	3100 2350 3200 2350
Wire Wire Line
	3100 2450 3200 2450
Wire Wire Line
	3100 2550 3200 2550
Wire Wire Line
	3100 2650 3200 2650
Wire Wire Line
	3100 2750 3200 2750
Wire Wire Line
	3100 2850 3200 2850
Wire Wire Line
	3100 2950 3200 2950
Wire Wire Line
	3100 3050 3200 3050
Wire Wire Line
	3100 3150 3200 3150
Wire Wire Line
	3100 3250 3200 3250
Wire Wire Line
	3100 3350 3200 3350
Text Label 3200 3050 0    50   ~ 0
OUT12
Wire Wire Line
	4050 1700 3950 1700
Wire Wire Line
	4050 1800 3950 1800
Wire Wire Line
	4050 1900 3950 1900
Wire Wire Line
	4050 2100 3950 2100
Wire Wire Line
	4050 2200 3950 2200
Wire Wire Line
	4050 2300 3950 2300
Wire Wire Line
	4050 2500 3950 2500
Wire Wire Line
	4050 2600 3950 2600
Wire Wire Line
	4050 2700 3950 2700
Wire Wire Line
	4050 3300 3950 3300
Wire Wire Line
	4050 3400 3950 3400
Wire Wire Line
	4050 3500 3950 3500
Text Label 3950 1700 2    50   ~ 0
OUT0
Text Label 3950 1800 2    50   ~ 0
OUT1
Text Label 3950 1900 2    50   ~ 0
OUT2
Text Label 3950 2100 2    50   ~ 0
OUT3
Text Label 3950 2200 2    50   ~ 0
OUT4
Text Label 3950 2300 2    50   ~ 0
OUT5
Text Label 3950 2500 2    50   ~ 0
OUT6
Text Label 3950 2600 2    50   ~ 0
OUT7
Text Label 3950 2700 2    50   ~ 0
OUT8
Text Label 3950 2900 2    50   ~ 0
OUT9
Text Label 3950 3000 2    50   ~ 0
OUT10
Text Label 3950 3100 2    50   ~ 0
OUT11
Text Label 3950 3300 2    50   ~ 0
OUT12
Text Label 3950 3400 2    50   ~ 0
OUT13
Text Label 3950 3500 2    50   ~ 0
OUT14
NoConn ~ 3200 3350
Text Label 7850 2250 0    50   ~ 0
OUT20
Text Label 7850 2350 0    50   ~ 0
OUT21
NoConn ~ 8100 3350
Text Label 8650 1700 2    50   ~ 0
OUT16
Wire Wire Line
	8750 1700 8650 1700
Wire Wire Line
	8750 1800 8650 1800
Wire Wire Line
	8750 1900 8650 1900
Wire Wire Line
	8750 2100 8650 2100
Wire Wire Line
	8750 2200 8650 2200
Wire Wire Line
	8750 2300 8650 2300
Wire Wire Line
	8750 2500 8650 2500
Wire Wire Line
	8750 2600 8650 2600
Wire Wire Line
	8750 2700 8650 2700
Wire Wire Line
	8750 2900 8650 2900
Wire Wire Line
	8750 3000 8650 3000
Wire Wire Line
	8750 3100 8650 3100
Wire Wire Line
	8750 3300 8650 3300
Wire Wire Line
	8750 3400 8650 3400
Wire Wire Line
	8750 3500 8650 3500
Text Label 8650 1800 2    50   ~ 0
OUT17
Text Label 8650 1900 2    50   ~ 0
OUT18
Text Label 8650 2100 2    50   ~ 0
OUT19
Text Label 8650 2200 2    50   ~ 0
OUT20
Text Label 8650 2300 2    50   ~ 0
OUT21
Text Label 8650 2500 2    50   ~ 0
OUT22
Text Label 8650 2600 2    50   ~ 0
OUT23
Text Label 8650 2700 2    50   ~ 0
OUT24
Text Label 8650 2900 2    50   ~ 0
OUT25
Text Label 8650 3000 2    50   ~ 0
OUT26
Text Label 8650 3100 2    50   ~ 0
OUT27
Text Label 8650 3300 2    50   ~ 0
OUT28
Text Label 8650 3400 2    50   ~ 0
OUT29
Text Label 8650 3500 2    50   ~ 0
OUT30
Wire Wire Line
	7800 1850 8100 1850
Wire Wire Line
	950  1050 4550 1050
Wire Wire Line
	5650 1000 9250 1000
Text Label 5650 3250 0    50   ~ 0
SIN_1
Text Label 5650 3350 0    50   ~ 0
SIN_2
Text Label 3200 2050 0    50   ~ 0
OUT2
Text Label 3200 1950 0    50   ~ 0
OUT1
Text Label 3200 1850 0    50   ~ 0
OUT0
Text Label 10400 3350 0    50   ~ 0
SIN_3
Text Label 10400 3250 0    50   ~ 0
SIN_2
Wire Wire Line
	10400 1000 14000 1000
Wire Wire Line
	12550 1850 12850 1850
Text Label 13400 3400 2    50   ~ 0
OUT45
Text Label 13400 3300 2    50   ~ 0
OUT44
Text Label 13400 3100 2    50   ~ 0
OUT43
Text Label 13400 3000 2    50   ~ 0
OUT42
Text Label 13400 2900 2    50   ~ 0
OUT41
Text Label 13400 2700 2    50   ~ 0
OUT40
Text Label 13400 2600 2    50   ~ 0
OUT39
Text Label 13400 2500 2    50   ~ 0
OUT38
Text Label 13400 2300 2    50   ~ 0
OUT37
Text Label 13400 2200 2    50   ~ 0
OUT36
Text Label 13400 2100 2    50   ~ 0
OUT35
Text Label 13400 1900 2    50   ~ 0
OUT34
Text Label 13400 1800 2    50   ~ 0
OUT33
Wire Wire Line
	13500 3500 13400 3500
Wire Wire Line
	13500 3400 13400 3400
Wire Wire Line
	13500 3300 13400 3300
Wire Wire Line
	13500 3100 13400 3100
Wire Wire Line
	13500 3000 13400 3000
Wire Wire Line
	13500 2900 13400 2900
Wire Wire Line
	13500 2700 13400 2700
Wire Wire Line
	13500 2600 13400 2600
Wire Wire Line
	13500 2500 13400 2500
Wire Wire Line
	13500 2300 13400 2300
Wire Wire Line
	13500 2200 13400 2200
Wire Wire Line
	13500 2100 13400 2100
Wire Wire Line
	13500 1900 13400 1900
Wire Wire Line
	13500 1800 13400 1800
Wire Wire Line
	13500 1700 13400 1700
Text Label 13400 1700 2    50   ~ 0
OUT32
NoConn ~ 12850 3350
Text Label 12600 2350 0    50   ~ 0
OUT37
Text Label 12600 2250 0    50   ~ 0
OUT36
Wire Wire Line
	11850 1250 10400 1250
Wire Wire Line
	11850 1550 11850 1250
Wire Wire Line
	11150 1650 11150 1850
Text Label 10400 3950 0    50   ~ 0
5V
Wire Wire Line
	11750 3950 10400 3950
$Comp
L Device:C C6
U 1 1 5D255A67
P 11750 3800
F 0 "C6" H 11865 3846 50  0000 L CNN
F 1 "0.1uF" H 11865 3755 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 11788 3650 50  0001 C CNN
F 3 "~" H 11750 3800 50  0001 C CNN
	1    11750 3800
	-1   0    0    1   
$EndComp
Text Label 10400 3650 0    50   ~ 0
GND
Wire Wire Line
	11750 3650 10400 3650
Connection ~ 11750 3650
Wire Wire Line
	11850 3650 11750 3650
Wire Wire Line
	11150 3350 10400 3350
$Comp
L Driver_LED:TLC5940PWP U4
U 1 1 5D255A58
P 11850 2550
F 0 "U4" H 11850 3731 50  0000 C CNN
F 1 "TLC5940PWP" H 11850 3640 50  0000 C CNN
F 2 "Arduino_TLC5940_Shield:TSSOP-28-1EP_4.4x9.7mm_Pitch0.65mm" H 11875 1575 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/tlc5940.pdf" H 11450 3250 50  0001 C CNN
	1    11850 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	12550 1950 12850 1950
Wire Wire Line
	12550 2050 12850 2050
Wire Wire Line
	12550 2150 12850 2150
Wire Wire Line
	12550 2250 12850 2250
Wire Wire Line
	12550 2350 12850 2350
Wire Wire Line
	12550 2450 12850 2450
Wire Wire Line
	12550 2550 12850 2550
Wire Wire Line
	12550 2650 12850 2650
Wire Wire Line
	12550 2750 12850 2750
Wire Wire Line
	12550 2850 12850 2850
Wire Wire Line
	12550 2950 12850 2950
Wire Wire Line
	12550 3050 12850 3050
Wire Wire Line
	12550 3150 12850 3150
Wire Wire Line
	12550 3250 12850 3250
Wire Wire Line
	12550 3350 12850 3350
Text Notes 12550 1450 0    50   ~ 0
LED’s connect here
Wire Wire Line
	10750 1650 11150 1650
Wire Wire Line
	10500 1650 10750 1650
Connection ~ 10750 1650
$Comp
L Device:R R4
U 1 1 5D255A3E
P 10750 1800
F 0 "R4" V 10543 1800 50  0000 C CNN
F 1 "1K VAR" V 10634 1800 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 10680 1800 50  0001 C CNN
F 3 "~" H 10750 1800 50  0001 C CNN
	1    10750 1800
	-1   0    0    1   
$EndComp
Wire Wire Line
	10750 1950 11150 1950
$Comp
L power:GND #PWR03
U 1 1 5D255A33
P 10500 1650
F 0 "#PWR03" H 10500 1400 50  0001 C CNN
F 1 "GND" V 10505 1522 50  0000 R CNN
F 2 "" H 10500 1650 50  0001 C CNN
F 3 "" H 10500 1650 50  0001 C CNN
	1    10500 1650
	0    1    1    0   
$EndComp
Wire Wire Line
	10400 3250 11150 3250
NoConn ~ 11150 2750
Wire Wire Line
	11150 2050 10400 2050
Text Label 10400 2050 0    50   ~ 0
5V
Wire Wire Line
	11150 2150 10400 2150
Text Label 10400 2150 0    50   ~ 0
GSCLK
Wire Wire Line
	11150 2350 10400 2350
Text Label 10400 2350 0    50   ~ 0
BLANK
Wire Wire Line
	11150 2450 10400 2450
Text Label 10400 2450 0    50   ~ 0
XLAT
Wire Wire Line
	11150 3150 10400 3150
Text Label 10400 3150 0    50   ~ 0
SCLK
Text Label 12600 1850 0    50   ~ 0
OUT32
Text Label 12600 1950 0    50   ~ 0
OUT33
Text Label 12600 2150 0    50   ~ 0
OUT35
Text Label 12600 2450 0    50   ~ 0
OUT38
Text Label 12600 2550 0    50   ~ 0
OUT39
Text Label 12600 2650 0    50   ~ 0
OUT40
Text Label 12600 2750 0    50   ~ 0
OUT41
Text Label 12600 2850 0    50   ~ 0
OUT42
Text Label 12600 2950 0    50   ~ 0
OUT43
Text Label 12600 3050 0    50   ~ 0
OUT44
Text Label 12600 3150 0    50   ~ 0
OUT45
Text Label 12600 3250 0    50   ~ 0
OUT46
Text Label 11000 1950 0    50   ~ 0
IREF_3
Text Label 10400 1000 0    50   ~ 0
5V_PWR
Text Label 10400 1250 0    50   ~ 0
5V
Text Label 12600 2050 0    50   ~ 0
OUT34
Text Label 13400 3500 2    50   ~ 0
OUT46
Text Label 1000 6950 0    50   ~ 0
SIN_3
Wire Wire Line
	1000 4700 4600 4700
Wire Wire Line
	3150 5550 3450 5550
Text Label 4000 7100 2    50   ~ 0
OUT61
Text Label 4000 7000 2    50   ~ 0
OUT60
Text Label 4000 6800 2    50   ~ 0
OUT59
Text Label 4000 6700 2    50   ~ 0
OUT58
Text Label 4000 6600 2    50   ~ 0
OUT57
Text Label 4000 6400 2    50   ~ 0
OUT56
Text Label 4000 6300 2    50   ~ 0
OUT55
Text Label 4000 6200 2    50   ~ 0
OUT54
Text Label 4000 6000 2    50   ~ 0
OUT53
Text Label 4000 5900 2    50   ~ 0
OUT52
Text Label 4000 5800 2    50   ~ 0
OUT51
Text Label 4000 5600 2    50   ~ 0
OUT50
Text Label 4000 5500 2    50   ~ 0
OUT49
Wire Wire Line
	4100 7200 4000 7200
Wire Wire Line
	4100 7100 4000 7100
Wire Wire Line
	4100 7000 4000 7000
Wire Wire Line
	4100 6800 4000 6800
Wire Wire Line
	4100 6700 4000 6700
Wire Wire Line
	4100 6600 4000 6600
Wire Wire Line
	4100 6400 4000 6400
Wire Wire Line
	4100 6300 4000 6300
Wire Wire Line
	4100 6200 4000 6200
Wire Wire Line
	4100 6000 4000 6000
Wire Wire Line
	4100 5900 4000 5900
Wire Wire Line
	4100 5800 4000 5800
Wire Wire Line
	4100 5600 4000 5600
Wire Wire Line
	4100 5500 4000 5500
Wire Wire Line
	4100 5400 4000 5400
Text Label 4000 5400 2    50   ~ 0
OUT48
NoConn ~ 3450 7050
Text Label 3200 6050 0    50   ~ 0
OUT53
Text Label 3200 5950 0    50   ~ 0
OUT52
Wire Wire Line
	2450 4950 1000 4950
Wire Wire Line
	2450 5250 2450 4950
Wire Wire Line
	1750 5350 1750 5550
Text Label 1000 7650 0    50   ~ 0
5V
Wire Wire Line
	2350 7650 1000 7650
$Comp
L Device:C C5
U 1 1 5D2A9A55
P 2350 7500
F 0 "C5" H 2465 7546 50  0000 L CNN
F 1 "0.1uF" H 2465 7455 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 2388 7350 50  0001 C CNN
F 3 "~" H 2350 7500 50  0001 C CNN
	1    2350 7500
	-1   0    0    1   
$EndComp
Text Label 1000 7350 0    50   ~ 0
GND
Wire Wire Line
	2350 7350 1000 7350
Connection ~ 2350 7350
Wire Wire Line
	2450 7350 2350 7350
Wire Wire Line
	1750 7050 1000 7050
$Comp
L Driver_LED:TLC5940PWP U3
U 1 1 5D2A9A64
P 2450 6250
F 0 "U3" H 2450 7431 50  0000 C CNN
F 1 "TLC5940PWP" H 2450 7340 50  0000 C CNN
F 2 "Arduino_TLC5940_Shield:TSSOP-28-1EP_4.4x9.7mm_Pitch0.65mm" H 2475 5275 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/tlc5940.pdf" H 2050 6950 50  0001 C CNN
	1    2450 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 5650 3450 5650
Wire Wire Line
	3150 5750 3450 5750
Wire Wire Line
	3150 5850 3450 5850
Wire Wire Line
	3150 5950 3450 5950
Wire Wire Line
	3150 6050 3450 6050
Wire Wire Line
	3150 6150 3450 6150
Wire Wire Line
	3150 6250 3450 6250
Wire Wire Line
	3150 6350 3450 6350
Wire Wire Line
	3150 6450 3450 6450
Wire Wire Line
	3150 6550 3450 6550
Wire Wire Line
	3150 6650 3450 6650
Wire Wire Line
	3150 6750 3450 6750
Wire Wire Line
	3150 6850 3450 6850
Wire Wire Line
	3150 6950 3450 6950
Wire Wire Line
	3150 7050 3450 7050
Text Notes 3150 5150 0    50   ~ 0
LED’s connect here
Wire Wire Line
	1350 5350 1750 5350
Wire Wire Line
	1100 5350 1350 5350
Connection ~ 1350 5350
$Comp
L Device:R R3
U 1 1 5D2A9A81
P 1350 5500
F 0 "R3" V 1143 5500 50  0000 C CNN
F 1 "1K VAR" V 1234 5500 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 1280 5500 50  0001 C CNN
F 3 "~" H 1350 5500 50  0001 C CNN
	1    1350 5500
	-1   0    0    1   
$EndComp
Wire Wire Line
	1350 5650 1750 5650
$Comp
L power:GND #PWR02
U 1 1 5D2A9A8C
P 1100 5350
F 0 "#PWR02" H 1100 5100 50  0001 C CNN
F 1 "GND" V 1105 5222 50  0000 R CNN
F 2 "" H 1100 5350 50  0001 C CNN
F 3 "" H 1100 5350 50  0001 C CNN
	1    1100 5350
	0    1    1    0   
$EndComp
Wire Wire Line
	1000 6950 1750 6950
NoConn ~ 1750 6450
Wire Wire Line
	1750 5750 1000 5750
Text Label 1000 5750 0    50   ~ 0
5V
Wire Wire Line
	1750 5850 1000 5850
Text Label 1000 5850 0    50   ~ 0
GSCLK
Wire Wire Line
	1750 6050 1000 6050
Text Label 1000 6050 0    50   ~ 0
BLANK
Wire Wire Line
	1750 6150 1000 6150
Text Label 1000 6150 0    50   ~ 0
XLAT
Wire Wire Line
	1750 6850 1000 6850
Text Label 1000 6850 0    50   ~ 0
SCLK
Text Label 3200 5550 0    50   ~ 0
OUT48
Text Label 3200 5650 0    50   ~ 0
OUT49
Text Label 3200 5850 0    50   ~ 0
OUT51
Text Label 3200 6150 0    50   ~ 0
OUT54
Text Label 3200 6250 0    50   ~ 0
OUT55
Text Label 3200 6350 0    50   ~ 0
OUT56
Text Label 3200 6450 0    50   ~ 0
OUT57
Text Label 3200 6550 0    50   ~ 0
OUT58
Text Label 3200 6650 0    50   ~ 0
OUT59
Text Label 3200 6750 0    50   ~ 0
OUT60
Text Label 3200 6850 0    50   ~ 0
OUT61
Text Label 3200 6950 0    50   ~ 0
OUT62
Text Label 1600 5650 0    50   ~ 0
IREF_4
Text Label 1000 4700 0    50   ~ 0
5V_PWR
Text Label 1000 4950 0    50   ~ 0
5V
Text Label 3200 5750 0    50   ~ 0
OUT50
Text Label 4000 7200 2    50   ~ 0
OUT62
NoConn ~ 1000 7050
Wire Wire Line
	12900 7800 12150 7800
Text Label 12150 7800 0    50   ~ 0
SWITCH_IN
$Comp
L Device:R R5
U 1 1 5D33D6E2
P 10850 7100
F 0 "R5" V 10643 7100 50  0000 C CNN
F 1 "10K" V 10734 7100 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 10780 7100 50  0001 C CNN
F 3 "~" H 10850 7100 50  0001 C CNN
	1    10850 7100
	0    1    1    0   
$EndComp
Text Label 9800 7100 0    50   ~ 0
5V
Wire Wire Line
	11000 7100 11350 7100
Text Label 11350 7100 2    50   ~ 0
GND
Wire Wire Line
	10700 7100 10550 7100
$Comp
L Connector_Generic:Conn_01x02 J26
U 1 1 5D33F28F
P 10300 7300
F 0 "J26" V 10172 7380 50  0000 L CNN
F 1 "Conn_01x02" V 10263 7380 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 10300 7300 50  0001 C CNN
F 3 "~" H 10300 7300 50  0001 C CNN
	1    10300 7300
	0    1    1    0   
$EndComp
Wire Wire Line
	10200 7100 9800 7100
Wire Wire Line
	10550 7100 10550 6750
Connection ~ 10550 7100
Wire Wire Line
	10550 7100 10300 7100
Text Label 10550 6750 0    50   ~ 0
SWITCH_IN
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J3
U 1 1 5D4C856E
P 4250 1800
F 0 "J3" H 4300 2117 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 4300 2026 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03_Pitch2.54mm_SMD" H 4250 1800 50  0001 C CNN
F 3 "~" H 4250 1800 50  0001 C CNN
	1    4250 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 1050 4550 1700
Wire Wire Line
	4550 1700 4550 1800
Connection ~ 4550 1700
Connection ~ 4550 1900
Connection ~ 4550 1800
Wire Wire Line
	4550 1800 4550 1900
Wire Wire Line
	9250 1000 9250 1700
Wire Wire Line
	14000 1000 14000 1700
Wire Wire Line
	4600 4700 4600 5400
Wire Wire Line
	4550 1900 4550 2100
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J7
U 1 1 5D6EECF6
P 4250 2200
F 0 "J7" H 4300 2517 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 4300 2426 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03_Pitch2.54mm_SMD" H 4250 2200 50  0001 C CNN
F 3 "~" H 4250 2200 50  0001 C CNN
	1    4250 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 2300 4550 2500
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J8
U 1 1 5D6EF504
P 4250 2600
F 0 "J8" H 4300 2917 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 4300 2826 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03_Pitch2.54mm_SMD" H 4250 2600 50  0001 C CNN
F 3 "~" H 4250 2600 50  0001 C CNN
	1    4250 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 2700 4550 2900
Wire Wire Line
	4050 3100 3950 3100
Wire Wire Line
	4050 3000 3950 3000
Wire Wire Line
	4050 2900 3950 2900
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J9
U 1 1 5D6F05D3
P 4250 3000
F 0 "J9" H 4300 3317 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 4300 3226 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03_Pitch2.54mm_SMD" H 4250 3000 50  0001 C CNN
F 3 "~" H 4250 3000 50  0001 C CNN
	1    4250 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 3100 4550 3300
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J10
U 1 1 5D6F0FA4
P 4250 3400
F 0 "J10" H 4300 3717 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 4300 3626 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03_Pitch2.54mm_SMD" H 4250 3400 50  0001 C CNN
F 3 "~" H 4250 3400 50  0001 C CNN
	1    4250 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 3400 4550 3500
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J16
U 1 1 5D6F7A2E
P 8950 1800
F 0 "J16" H 9000 2117 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 9000 2026 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03_Pitch2.54mm_SMD" H 8950 1800 50  0001 C CNN
F 3 "~" H 8950 1800 50  0001 C CNN
	1    8950 1800
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J17
U 1 1 5D6F7A38
P 8950 2200
F 0 "J17" H 9000 2517 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 9000 2426 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03_Pitch2.54mm_SMD" H 8950 2200 50  0001 C CNN
F 3 "~" H 8950 2200 50  0001 C CNN
	1    8950 2200
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J18
U 1 1 5D6F7A42
P 8950 2600
F 0 "J18" H 9000 2917 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 9000 2826 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03_Pitch2.54mm_SMD" H 8950 2600 50  0001 C CNN
F 3 "~" H 8950 2600 50  0001 C CNN
	1    8950 2600
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J19
U 1 1 5D6F7A4C
P 8950 3000
F 0 "J19" H 9000 3317 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 9000 3226 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03_Pitch2.54mm_SMD" H 8950 3000 50  0001 C CNN
F 3 "~" H 8950 3000 50  0001 C CNN
	1    8950 3000
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J20
U 1 1 5D6F7A56
P 8950 3400
F 0 "J20" H 9000 3717 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 9000 3626 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03_Pitch2.54mm_SMD" H 8950 3400 50  0001 C CNN
F 3 "~" H 8950 3400 50  0001 C CNN
	1    8950 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	9250 1900 9250 2100
Wire Wire Line
	9250 2300 9250 2500
Wire Wire Line
	9250 2700 9250 2900
Wire Wire Line
	9250 3100 9250 3300
Wire Wire Line
	9250 3400 9250 3500
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J21
U 1 1 5D716C4B
P 13700 1800
F 0 "J21" H 13750 2117 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 13750 2026 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03_Pitch2.54mm_SMD" H 13700 1800 50  0001 C CNN
F 3 "~" H 13700 1800 50  0001 C CNN
	1    13700 1800
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J22
U 1 1 5D716C55
P 13700 2200
F 0 "J22" H 13750 2517 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 13750 2426 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03_Pitch2.54mm_SMD" H 13700 2200 50  0001 C CNN
F 3 "~" H 13700 2200 50  0001 C CNN
	1    13700 2200
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J23
U 1 1 5D716C5F
P 13700 2600
F 0 "J23" H 13750 2917 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 13750 2826 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03_Pitch2.54mm_SMD" H 13700 2600 50  0001 C CNN
F 3 "~" H 13700 2600 50  0001 C CNN
	1    13700 2600
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J24
U 1 1 5D716C69
P 13700 3000
F 0 "J24" H 13750 3317 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 13750 3226 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03_Pitch2.54mm_SMD" H 13700 3000 50  0001 C CNN
F 3 "~" H 13700 3000 50  0001 C CNN
	1    13700 3000
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J25
U 1 1 5D716C73
P 13700 3400
F 0 "J25" H 13750 3717 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 13750 3626 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03_Pitch2.54mm_SMD" H 13700 3400 50  0001 C CNN
F 3 "~" H 13700 3400 50  0001 C CNN
	1    13700 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	14000 1900 14000 2100
Wire Wire Line
	14000 2300 14000 2500
Wire Wire Line
	14000 2700 14000 2900
Wire Wire Line
	14000 3100 14000 3300
Wire Wire Line
	14000 3400 14000 3500
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J11
U 1 1 5D739EB4
P 4300 5500
F 0 "J11" H 4350 5817 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 4350 5726 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03_Pitch2.54mm_SMD" H 4300 5500 50  0001 C CNN
F 3 "~" H 4300 5500 50  0001 C CNN
	1    4300 5500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J12
U 1 1 5D739EBE
P 4300 5900
F 0 "J12" H 4350 6217 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 4350 6126 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03_Pitch2.54mm_SMD" H 4300 5900 50  0001 C CNN
F 3 "~" H 4300 5900 50  0001 C CNN
	1    4300 5900
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J13
U 1 1 5D739EC8
P 4300 6300
F 0 "J13" H 4350 6617 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 4350 6526 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03_Pitch2.54mm_SMD" H 4300 6300 50  0001 C CNN
F 3 "~" H 4300 6300 50  0001 C CNN
	1    4300 6300
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J14
U 1 1 5D739ED2
P 4300 6700
F 0 "J14" H 4350 7017 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 4350 6926 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03_Pitch2.54mm_SMD" H 4300 6700 50  0001 C CNN
F 3 "~" H 4300 6700 50  0001 C CNN
	1    4300 6700
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J15
U 1 1 5D739EDC
P 4300 7100
F 0 "J15" H 4350 7417 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 4350 7326 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03_Pitch2.54mm_SMD" H 4300 7100 50  0001 C CNN
F 3 "~" H 4300 7100 50  0001 C CNN
	1    4300 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 5600 4600 5800
Wire Wire Line
	4600 6000 4600 6200
Wire Wire Line
	4600 6400 4600 6600
Wire Wire Line
	4600 6800 4600 7000
Wire Wire Line
	4600 7100 4600 7200
Wire Wire Line
	4550 2100 4550 2200
Connection ~ 4550 2100
Wire Wire Line
	4550 2200 4550 2300
Connection ~ 4550 2200
Connection ~ 4550 2300
Wire Wire Line
	4550 2500 4550 2600
Connection ~ 4550 2500
Wire Wire Line
	4550 2600 4550 2700
Connection ~ 4550 2600
Connection ~ 4550 2700
Wire Wire Line
	4550 2900 4550 3000
Connection ~ 4550 2900
Wire Wire Line
	4550 3100 4550 3000
Connection ~ 4550 3100
Connection ~ 4550 3000
Wire Wire Line
	4550 3400 4550 3300
Connection ~ 4550 3400
Connection ~ 4550 3300
Wire Wire Line
	9250 1700 9250 1800
Connection ~ 9250 1700
Wire Wire Line
	9250 1800 9250 1900
Connection ~ 9250 1800
Connection ~ 9250 1900
Wire Wire Line
	9250 2100 9250 2200
Connection ~ 9250 2100
Wire Wire Line
	9250 2200 9250 2300
Connection ~ 9250 2200
Connection ~ 9250 2300
Wire Wire Line
	9250 2500 9250 2600
Connection ~ 9250 2500
Wire Wire Line
	9250 2600 9250 2700
Connection ~ 9250 2600
Connection ~ 9250 2700
Wire Wire Line
	9250 2900 9250 3000
Connection ~ 9250 2900
Wire Wire Line
	9250 3000 9250 3100
Connection ~ 9250 3000
Connection ~ 9250 3100
Wire Wire Line
	9250 3300 9250 3400
Connection ~ 9250 3300
Connection ~ 9250 3400
Wire Wire Line
	14000 1700 14000 1800
Connection ~ 14000 1700
Wire Wire Line
	14000 1800 14000 1900
Connection ~ 14000 1800
Connection ~ 14000 1900
Wire Wire Line
	14000 2100 14000 2200
Connection ~ 14000 2100
Wire Wire Line
	14000 2200 14000 2300
Connection ~ 14000 2200
Connection ~ 14000 2300
Wire Wire Line
	14000 2500 14000 2600
Connection ~ 14000 2500
Wire Wire Line
	14000 2600 14000 2700
Connection ~ 14000 2600
Connection ~ 14000 2700
Wire Wire Line
	14000 2900 14000 3000
Connection ~ 14000 2900
Wire Wire Line
	14000 3000 14000 3100
Connection ~ 14000 3000
Connection ~ 14000 3100
Wire Wire Line
	14000 3300 14000 3400
Connection ~ 14000 3300
Connection ~ 14000 3400
Wire Wire Line
	4600 5400 4600 5500
Connection ~ 4600 5400
Wire Wire Line
	4600 5500 4600 5600
Connection ~ 4600 5500
Connection ~ 4600 5600
Wire Wire Line
	4600 5800 4600 5900
Connection ~ 4600 5800
Wire Wire Line
	4600 5900 4600 6000
Connection ~ 4600 5900
Connection ~ 4600 6000
Wire Wire Line
	4600 6200 4600 6300
Connection ~ 4600 6200
Wire Wire Line
	4600 6300 4600 6400
Connection ~ 4600 6300
Connection ~ 4600 6400
Wire Wire Line
	4600 6600 4600 6700
Connection ~ 4600 6600
Wire Wire Line
	4600 6700 4600 6800
Connection ~ 4600 6700
Connection ~ 4600 6800
Wire Wire Line
	4600 7000 4600 7100
Connection ~ 4600 7000
Connection ~ 4600 7100
$Comp
L Device:LED D1
U 1 1 5CF430C0
P 10350 8850
F 0 "D1" H 10343 9066 50  0000 C CNN
F 1 "LED" H 10343 8975 50  0000 C CNN
F 2 "LEDs:LED_1206_HandSoldering" H 10350 8850 50  0001 C CNN
F 3 "~" H 10350 8850 50  0001 C CNN
	1    10350 8850
	1    0    0    -1  
$EndComp
Text Notes 10400 8350 0    50   ~ 0
Status LED
$Comp
L Device:R R6
U 1 1 5CF46035
P 10850 8850
F 0 "R6" V 10643 8850 50  0000 C CNN
F 1 "220" V 10734 8850 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 10780 8850 50  0001 C CNN
F 3 "~" H 10850 8850 50  0001 C CNN
	1    10850 8850
	0    1    1    0   
$EndComp
Wire Wire Line
	10500 8850 10700 8850
Wire Wire Line
	10200 8850 9850 8850
Wire Wire Line
	11000 8850 11700 8850
Text Label 9850 8850 0    50   ~ 0
GND
Text Label 11650 8850 2    50   ~ 0
5V
$EndSCHEMATC
